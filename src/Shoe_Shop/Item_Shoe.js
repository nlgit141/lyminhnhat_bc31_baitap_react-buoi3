import React, { Component } from 'react';
export default class Item_Shoe extends Component {


    render() {
        let { name, price, description, image } = this.props.dataShoe;

        return (
            <div className='col-3 p-1' style={{ background: "black" }}>
                <div className='p-1' >
                    <div className="card" style={{ width: '100%', height: "500px" }}>
                        <img src={image} className="card-img-top" alt="..." />
                        <div className="card-body">
                            <div style={{ width: "100%" }}>
                                <h5 className="card-title" style={{ height: "30px" }}>{name}</h5>
                            </div>
                            <p style={{ marginTop: "25px" }}>${price}</p>
                            <p className="card-text">{description.length < 30 ? description : description.slice(1, 40) + "..."}</p>
                            <button onClick={() => {
                                this.props.clickAddToCart(this.props.dataShoe);
                            }} className="btn btn-dark">Add to card</button>
                        </div>
                    </div>
                </div>
            </div >


        );
    }
}
