import React, { Component } from 'react';
import shortid from 'shortid';



export default class Table_Shoe extends Component {
    renderTableCart = () => {
        return this.props.giohang.map((item) => {
            let randomKey = shortid.generate();
            return (<tr key={randomKey}>
                <th>{item.id}</th>
                <th>{item.name}</th>
                <th><img src={item.image} style={{ width: "35px" }} type="" /></th>
                <th>${item.price}</th>
                <th>
                    <button className='btn btn-danger' onClick={() => {
                        this.props.handleStep(item.id, -1);
                    }}>-</button>
                    <span className='mx-2'>{item.soLuong}</span>
                    <button
                        onClick={() => {
                            this.props.handleStep(item.id, +1);
                        }} className='btn btn-warning'>+</button>
                </th>
                {/* <th><input type="text" value={`$${item.tongTien}`} /></th> */}
                <th><button className='btn btn-dark'>${item.tongTien}</button></th>
                <td>
                    <button onClick={() => {
                        this.props.handleRemove(item.id);
                    }} className='btn btn-danger'>delete</button>
                </td>
            </tr >);
        });
    };

    render() {
        return (
            <div >
                <table className='table'>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NAME</th>
                            <th>IMAGE</th>
                            <th>PRICE</th>
                            <th>QUANTITY</th>
                            <th>TOTAL</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTableCart()}
                    </tbody>
                </table>
            </div>
        );
    }
}
