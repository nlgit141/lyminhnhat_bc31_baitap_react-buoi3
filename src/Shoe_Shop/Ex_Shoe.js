import React, { Component } from 'react';
import { ShoeArr } from './Datashoe';
import Item_Shoe from './Item_Shoe';
import Table_Shoe from './Table_Shoe';
import shortid from 'shortid';

export default class Ex_Shoe extends Component {
    state = {
        shoe: ShoeArr,
        gioHang: [],
    };


    handleAddToCart = (sp) => {
        console.log("object");
        let index = this.state.gioHang.findIndex((item) => {
            return item.id == sp.id;
        });
        let cloneGioHang = [...this.state.gioHang];
        console.log('cloneGioHang: ', cloneGioHang);

        if (index == -1) {
            let newSP = { ...sp, soLuong: 1, tongTien: 0 };
            newSP.tongTien = newSP.price * newSP.soLuong;
            cloneGioHang.push(newSP);
        } else {
            cloneGioHang[index].soLuong++;
            cloneGioHang[index].tongTien = cloneGioHang[index].price * cloneGioHang[index].soLuong;
        } this.setState({
            gioHang: cloneGioHang
        });
        console.log("object", index);
    };

    handleRemove = (idSp) => {
        console.log("object");
        let index = this.state.gioHang.findIndex((item) => {
            return item.id == idSp;
        });
        if (index !== -1) {
            let cloneGioHang = [...this.state.gioHang];
            cloneGioHang.splice(index, 1);
            this.setState({
                gioHang: cloneGioHang
            });
        }
    };


    handleStepShoe = (indexSp, step) => {
        let index = this.state.gioHang.findIndex((item) => {

            return (item.id == indexSp);
        });
        let cloneGioHang = [...this.state.gioHang];
        if (index !== -1) {
            cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + step;
            cloneGioHang[index].tongTien = cloneGioHang[index].tongTien + cloneGioHang[index].price * step;
        }
        if (cloneGioHang[index].soLuong == 0) {
            cloneGioHang.splice(index, 1);
        }
        this.setState({
            gioHang: cloneGioHang
        });
        console.log("index", this.state.gioHang);
    };

    renderItemShoe = () => {
        return this.state.shoe.map((item) => {
            let randomKey = shortid.generate();
            return <Item_Shoe dataShoe={item} key={randomKey} clickAddToCart={this.handleAddToCart} />;
        });
    };


    render() {
        return (
            <div className="container">
                <h1 className='py-3'>Shoe-Shop</h1>
                <div>
                    {this.state.gioHang.length > 0 && <Table_Shoe
                        giohang={this.state.gioHang}
                        handleRemove={this.handleRemove}
                        handleStep={this.handleStepShoe}
                    />}
                </div>
                <div className="row">{this.renderItemShoe()}</div>
            </div>
        );
    }
}
