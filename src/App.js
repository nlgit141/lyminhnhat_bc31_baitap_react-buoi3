import './App.css';
import Phone from './Phone_store/Phone';
import Ex_Shoe from './Shoe_Shop/Ex_Shoe';

function App() {
  return (
    <div className="App">
      <Ex_Shoe />
      <Phone />
    </div>
  );
}

export default App;
