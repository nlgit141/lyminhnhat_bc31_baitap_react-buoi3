import React, { Component } from 'react';

export default class Item_Phone extends Component {
    render() {
        let { maSP, tenSP, manHinh, heDieuHanh, cameraTruoc, cameraSau, ram, rom, giaBan, hinhAnh,
        } = this.props.renderItemPhone;
        return (
            <div className='col-4'>
                <div className="card" style={{ width: '18rem' }}>
                    <img src={hinhAnh} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{tenSP}</h5>
                        <p className="card-text">{heDieuHanh}</p>
                        <p className="card-text">{manHinh}</p>
                        <p className="card-text">{cameraTruoc}</p>
                        <p className="card-text">{cameraSau}</p>
                        <p className="card-text">{ram}</p>
                        <p className="card-text">{rom}</p>
                        <p className="card-text">{giaBan}</p>
                        <button onClick={() => {
                            this.props.onclickChangeDetail(this.props.renderItemPhone);
                        }} className="btn btn-primary">chi tiết sản phẩm</button>
                    </div>
                </div>
            </div>
        );
    }
}
