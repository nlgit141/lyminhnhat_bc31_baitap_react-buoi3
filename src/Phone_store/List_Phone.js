import React, { Component } from 'react';
import shortid from 'shortid';
import Item_Phone from './Item_Phone';


export default class List_Phone extends Component {
    renderItemPhone = () => {
        return this.props.renderListPhone.map((item) => {
            let randomKey = shortid.generate();
            return <Item_Phone onclickChangeDetail={this.props.changeDetail} renderItemPhone={item} key={randomKey} />;
        });
    };
    render() {

        return (
            <div className='container'>
                <div className='row'>{this.renderItemPhone()}</div>
            </div>
        );
    }
}
