import React, { Component } from 'react';
import { phoneArr } from './Data_Phone';
import Detail_Phone from './Detail_Phone';
import List_Phone from './List_Phone';

export default class Phone extends Component {
    state = {
        phone: phoneArr,
        detailPhone: phoneArr[0]
    };
    changeDetail = (index) => {
        this.setState({
            detailPhone: index
        });
    };

    render() {
        return (
            <div className='container py-5'>
                <h1 className='py-3'>STORE - PHONE</h1>
                <List_Phone renderListPhone={this.state.phone} changeDetail={this.changeDetail} />
                <Detail_Phone detailPhone={this.state.detailPhone} />
            </div>
        );
    }
}
