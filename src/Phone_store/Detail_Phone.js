import React, { Component } from 'react';

export default class Detail_Phone extends Component {
    render() {
        let { maSP, tenSP, manHinh, heDieuHanh, cameraTruoc, cameraSau, ram, rom, giaBan, hinhAnh } = this.props.detailPhone;
        return (
            <div className="container d-flex py-5" >
                <img src={hinhAnh} style={{ width: "50%" }} alt="" />
                <div style={{ textAlign: "left", fontSize: "20px" }} className="mt-5">
                    <p>Mã sản phẩm: {maSP}</p>
                    <p>Tên sản phẩm: {tenSP}</p>
                    <p>Màn hình: {manHinh}</p>
                    <p>Hệ điều hành: {heDieuHanh}</p>
                    <p>Camera trước: {cameraTruoc}</p>
                    <p>Camera sau: {cameraSau}</p>
                    <p>Ram: {ram}</p>
                    <p>Rom: {rom}</p>
                    <p>Giá bán: {giaBan}</p>
                </div>
            </div>
        );
    }
}
